resource "aws_security_group" "lb_sg" {
  name                   = "${var.name}-lb-sg"
  vpc_id                 = var.vpc_id
  description            = "${var.name}-lb-sg"
  revoke_rules_on_delete = true
  tags                   = var.tags
  dynamic "ingress" {
    for_each = local.lb_ingress_rules
    content {
      protocol        = ingress.value["protocol"]
      cidr_blocks     = ingress.value["cidr_blocks"]
      from_port       = ingress.value["from_port"]
      to_port         = ingress.value["to_port"]
      security_groups = ingress.value["security_groups"]

    }
  }
  dynamic "egress" {
    for_each = var.lb_egress_rules
    content {
      protocol        = egress.value["protocol"]
      cidr_blocks     = egress.value["cidr_blocks"]
      from_port       = egress.value["from_port"]
      to_port         = egress.value["to_port"]
      security_groups = egress.value["security_groups"]
    }
  }
}

resource "aws_security_group_rule" "vm_sg" {
  count                    = length(local.repository_ports)
  type                     = "ingress"
  from_port                = local.repository_ports[count.index]
  to_port                  = local.repository_ports[count.index]
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.lb_sg.id
  security_group_id        = var.vm_sg_id
}