module "alb" {
  source                           = "terraform-aws-modules/alb/aws"
  version                          = "7.0.0"
  create_lb                        = true
  access_logs                      = {}
  enable_cross_zone_load_balancing = true
  enable_deletion_protection       = false
  name                             = "${var.name}-lb"
  load_balancer_type               = "application"
  vpc_id                           = var.vpc_id
  subnets                          = var.lb_subnets
  tags                             = var.tags
  target_group_tags                = var.tags
  lb_tags                          = var.tags
  https_listeners_tags             = var.tags
  https_listener_rules_tags        = var.tags
  http_tcp_listener_rules_tags     = var.tags
  http_tcp_listeners_tags          = var.tags
  security_groups                  = [aws_security_group.lb_sg.id]
  internal                         = var.is_lb_internal
  https_listener_rules             = []
  http_tcp_listener_rules          = []

  https_listeners = local.https_listeners

  http_tcp_listeners = [
    {
      port        = 80
      protocol    = "HTTP"
      action_type = "redirect"
      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
  ]

  target_groups = local.target_groups
}
