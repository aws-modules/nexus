#!/bin/bash
sudo docker ps -q -a --filter name=nexus | xargs -r sudo docker stop nexus && sudo docker rm nexus
sudo docker run --log-opt max-size=10m --log-opt max-file=5  --restart always -d ${PUBLISH_PORTS} --name nexus -v ${NEXUS_DATA_DIR}:/nexus-data sonatype/nexus3:${NEXUS_VERSION}
