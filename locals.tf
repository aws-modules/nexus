locals {
  repository_ports         = [for r in var.repositories : r["port"] if lookup(r, "port", "") != ""]
  repository_publish_ports = [for p in local.repository_ports : "-p ${p}:${p}"]

  publish_ports = "-p ${var.nexus_port}:8081 ${join(" ", local.repository_publish_ports)}"
}

locals {
  https_listeners_repos = [
    for i in local.repository_ports :
    {
      port               = i
      protocol           = "HTTPS"
      certificate_arn    = module.acm.acm_certificate_arn
      target_group_index = index(local.repository_ports, i) + 1
    }
  ]
  https_listeners = concat([
    {
      port               = 443
      protocol           = "HTTPS"
      certificate_arn    = module.acm.acm_certificate_arn
      target_group_index = 0
    }
  ], local.https_listeners_repos)
}

locals {
  target_groups_repos = [
    for i in local.repository_ports :
    {
      name             = "${var.name}-tg-for-${i}"
      backend_protocol = "HTTP"
      backend_port     = i
      target_type      = "instance"
      tags             = var.tags
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = i
        healthy_threshold   = 3
        unhealthy_threshold = 3
        timeout             = 10
        protocol            = "HTTP"
        matcher             = "400"
      }
      targets = {
        my_target = {
          target_id = var.nexus_instance_id
          port      = i
        }
      }
    }
  ]
  target_groups = concat([
    {
      name             = "${var.name}-tg1"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
      tags             = var.tags
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = 80
        healthy_threshold   = 3
        unhealthy_threshold = 3
        timeout             = 10
        protocol            = "HTTP"
        matcher             = "200"
      }
      targets = {
        my_target = {
          target_id = var.nexus_instance_id
          port      = 80
        }
      }
    }
  ], local.target_groups_repos)
}

locals {
  lb_ingress_rules = merge({
    for i in local.repository_ports :
    i => {
      protocol        = "tcp"
      cidr_blocks     = ["0.0.0.0/0"]
      from_port       = i
      to_port         = i
      security_groups = null
    }
  }, var.lb_ingress_rules)
}