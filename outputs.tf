output "users" {
  value     = module.nexus_management.users
  sensitive = false
}

output "admin_password" {
  value     = random_password.admin_password.result
  sensitive = false
}
