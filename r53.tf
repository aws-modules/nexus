resource "aws_route53_record" "lb_this" {
  zone_id = var.dns_zone_id
  name    = var.lb_dns_name
  type    = "CNAME"
  ttl     = 60
  records = [module.alb.lb_dns_name]
}
