module "nexus_management" {
  source = "git@gitlab.com:varunelavia/nexus-management.git?ref=v1.0.0"

  nexus_port             = var.nexus_port
  nexus_host             = var.nexus_host
  blobs                  = var.blobs
  repositories           = var.repositories
  roles                  = var.roles
  users                  = var.users
  group_repo_members     = var.group_repo_members
  user_role_mapping      = var.user_role_mapping
  admin_password         = random_password.admin_password.result
  initial_admin_password = var.initial_admin_password
}

resource "random_password" "admin_password" {
  length  = 16
  special = false
}