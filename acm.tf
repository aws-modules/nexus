module "acm" {
  source              = "terraform-aws-modules/acm/aws"
  version             = "4.0.1"
  domain_name         = var.lb_dns_name
  zone_id             = var.dns_zone_id
  wait_for_validation = true
  tags                = var.tags
}