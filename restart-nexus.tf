data "template_file" "restart_nexus" {
  template = file("${path.module}/templates/restart_nexus.tpl")

  vars = {
    NEXUS_VERSION  = var.nexus_version
    PUBLISH_PORTS  = local.publish_ports
    NEXUS_DATA_DIR = var.nexus_data_dir
  }
}

# restart once to store the state, the next restart depends on the triggers
resource "null_resource" "restart_nexus" {
  triggers = {
    nexus_version = var.nexus_version
    publish_port  = local.publish_ports
  }

  connection {
    type        = "ssh"
    host        = var.nexus_host
    private_key = var.ssh_key
    port        = var.ssh_port
    user        = var.ssh_user
  }

  provisioner "remote-exec" {
    inline = [
      data.template_file.restart_nexus.rendered
    ]
  }

  depends_on = [
    module.nexus_management
  ]
}
