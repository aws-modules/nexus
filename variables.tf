variable "nexus_version" {
  description = "Nexus version"
  type        = string
}

variable "nexus_host" {
  description = "Nexus Host"
  type        = string
}

variable "nexus_port" {
  description = "Default nexus port"
  type        = number
}

variable "blobs" {
  description = "List of blobs to be managed"
  type        = list(string)
}

variable "repositories" {
  description = "Maps of repositories to be managed"
  type        = map(any)
}

variable "group_repo_members" {
  description = "Mapping of group repo to its member"
  type        = map(any)
}

variable "roles" {
  description = "Nexus roles to be managed"
  type        = map(any)
}

variable "users" {
  description = "Nexus users to be managed"
  type        = map(any)
}

variable "user_role_mapping" {
  description = "Nexus User to Role mapping"
  type        = map(any)
}

variable "initial_admin_password" {
  description = "Initial Admin Password of nexus, not used after password changed"
}

variable "ssh_port" {
  description = "SSH Port to nexus server"
  type        = number
}

variable "ssh_user" {
  description = "SSH User needed to perform nexus restart"
  type        = string
}

variable "ssh_key" {
  description = "SSH Key needed to perform nexus restart"
  type        = string
}

variable "nexus_data_dir" {
  type = string
}

variable "lb_egress_rules" {
  type = any
  default = {
    1 = {
      protocol        = "-1"
      cidr_blocks     = ["0.0.0.0/0"]
      from_port       = 0
      to_port         = 0
      security_groups = null
    }
  }
}

variable "lb_ingress_rules" {
  type = any
}

variable "lb_dns_name" {
  type = string
}

variable "is_lb_internal" {
  type = bool
}

variable "dns_zone_id" {
  type = string
}

variable "name" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "tags" {
  type = map(any)
}

variable "lb_subnets" {
  type = list(any)
}

variable "nexus_instance_id" {
  type = string
}

variable "vm_sg_id" {
  type = string
}